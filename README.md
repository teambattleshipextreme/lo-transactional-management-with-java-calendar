### Background

This repository should help you to get started with the Google Calendar API using Java and OAuth 2.0.

### Preliminaries

Using Google API's requires us to sign a license agreement, and register our application. Google has updated their API's for authorization to make them OAuth 2.0 compatible (which is not backwards compatible with OAuth 1.0!!). OAuth is a generic authorization standard, and might seem difficult to comprehend. Luckily, we only use it as a consumer, so we skip the theory. For those who are interested in security in enterprise applications, it is a good starting point to *google *for OAuth 2.0. In order to register your application and test it's functionality, do the following:

1. Create a new gmail account STUDID\_jee15@gmail.com:
[Google SignUp](https://accounts.google.com/SignUp)
Even if you already possess one, do it because at some point I might be able to see the user credentials.
2. Open the [Google API console](https://console.developers.google.com/flows/enableapi?apiid=calendar) and log into your google account.
3. Choose "Create a new project" and press continue. Choose as project name *STUDID\_jee5\_ass5* and keep the project- id that Google suggests. Click on "APIs &amp; auth" in the left pane, and choose "APIs". Create the project.
4. Go to the *OAuth consent screen* and select your new gmail address, as well as *STUDID\_jee5\_ass5* as the product name. Save it.
5. Go to Credentials and press *new credentials* and choose *OAuth client ID*. Select *other*, again type *STUDID\_jee5\_ass5*, and press *Create*. You will be presented with the applications Client ID and Client Secret.

### Instructions

The project within the repo contains two classes : *ApplicationTest* and *CalendarAuthorizer*. ApplicationTest contains compilation errors. Do the following:

1. Import the project into Eclipse.
2. Run CalendarAuthorizer and follow the instructions.
3. Resolve the compilation errors in ApplicationTest by entering your client ID, secret, and the refresh token from 2.
4. Run ApplicationTest. The test is expected to print the id and existing calendars for the test user. If it executes without exceptions, then you successfully completed the setup.
